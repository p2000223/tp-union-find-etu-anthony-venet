#include <iostream>

using namespace std;

class ElUnionFind
{
public:
    int val;
    ElUnionFind *parent;

    ElUnionFind(int _val = 0, ElUnionFind *_parent);

    const ElUnionFind racine();
};
