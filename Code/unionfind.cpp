#include <iostream>
#include <algorithm>

#include "unionfind.hpp"

using namespace std;

UnionFind::UnionFind(int size)
{
    for (int i = 0; i < size; i++)
    {
        unionTab.push_back(i);
    }
}

const int UnionFind::racine(int id)
{
    if (unionTab[id] == id)
    {
        return id;
    }
    else
    {
        return racine(unionTab[id]);
    }
}

bool UnionFind::recherche(int id1, int id2)
{
    return (racine(id1) == racine(id2));
}

void UnionFind::fusion(int id1, int id2)
{
    int root1 = racine(id1);
    int root2 = racine(id2);
    unionTab[root2] = root1;
}