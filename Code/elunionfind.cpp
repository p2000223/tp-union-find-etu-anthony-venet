#include <iostream>

#include "elunionfind.hpp"

using namespace std;


ElUnionFind::ElUnionFind(int _val = 0, ElUnionFind *_parent)
{
    val = _val;
    parent = this;
}

const ElUnionFind ElUnionFind::racine()
{
    if (parent == this)
    {
        return *this;
    }
    else
    {
        parent->racine();
    }   
}