#include <iostream>
#include "unionfind.cpp"
#include "labyrinthe.cpp"


using namespace std;

int main()
{
    UnionFind u(5);

    cout << (u.recherche(2, 3) ? "V" : "F") << endl;
    cout << (u.recherche(1, 1) ? "V" : "F") << endl;
    u.fusion(2, 3);
    cout << (u.recherche(2, 3) ? "V" : "F") << endl << endl;

}