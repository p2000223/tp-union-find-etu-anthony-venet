#include <iostream>
#include <vector>

using namespace std;

class UnionFind
{
public:
    vector<int> unionTab;

    UnionFind(int size);

    const int racine(int id);

    bool recherche(int id1, int id2);

    void fusion(int a, int b);
};